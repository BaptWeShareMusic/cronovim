" Navigation / layout
" -------------------
" Move to Split to the left
nnoremap <C-Left> :wincmd h<CR>
nnoremap <C-h> :wincmd h<CR>
" Move to Split to the right
nnoremap <C-Right> :wincmd l<CR>
nnoremap <C-l> :wincmd l<CR>
" Move to Split to the top
nnoremap <C-Up> :wincmd k<CR>
nnoremap <C-j> :wincmd k<CR>
" Move to Split to the bottom
nnoremap <C-Down> :wincmd j<CR>
nnoremap <C-k> :wincmd j<CR>
" Switch to previous buffer
nnoremap <A-Left> :bprevious<CR>
nnoremap <A-h> :bprevious<CR>
" Switch to next buffer
nnoremap <A-Right> :bnext<CR>
nnoremap <A-l> :bnext<CR>

" Vim related
nnoremap <Leader>s :nohlsearch<CR>
nnoremap <Leader>pp :Vrc<CR>
nnoremap <Leader>pl :VrcL<CR>
nnoremap <Leader>pr :call SourceAll()<CR>
nnoremap <Leader>p "+p
nnoremap <Leader>P "+P
vnoremap <Leader>p "+p
vnoremap <Leader>y "+y
nnoremap <Leader>bd :call CleanClose()<CR>
nnoremap <Leader>d :call CleanClose()<CR>
nnoremap <Leader>bl :lopen<CR>

" Plugin specific
" PHPActor
"nmap <Leader>lu :call phpactor#UseAdd()<CR>
"nmap <Leader>ld :call phpactor#GotoDefinition()<CR>

" LanguageClient
nnoremap <silent> <Leader>lk :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> <Leader>ld :call LanguageClient#textDocument_definition()<CR>

" NCM2 snippet extension (works together with utilSnip)
inoremap <silent> <expr> <Leader>e ncm2_ultisnips#expand_or("\<Leader>e", 'n')

" utilSnip
let g:UltiSnipsJumpForwardTrigger	= "<Leader>j"
let g:UltiSnipsJumpBackwardTrigger	= "<Leader>k"

" Tagbar
nnoremap <F3> :TagbarToggle<CR>

" FZF
nnoremap <Leader>o :FZF<CR>

" NERDTree
nnoremap <F2> :NERDTreeToggle<CR>

" YankRing
nnoremap <Leader>by :YRShow<CR>

" Scratch buffer
nnoremap <Leader>bs :Scratch<CR>
