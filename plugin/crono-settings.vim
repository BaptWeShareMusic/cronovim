" Vim airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

" NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" LanguageClient
let g:LanguageClient_serverCommands = {
    \ 'javascript': ['javascript-typescript-stdio'],
    \ 'html' : ['html-languageserver', '--stdio'],
    \ 'twig' : ['html-languageserver', '--stdio'],
    \ 'css' : ['css-languageserver', '--stdio'],
    \ 'scss' : ['css-languageserver', '--stdio'],
    \ 'php': ['php', '~/.config/nvim/plugged/LanguageServer-php-neovim/vendor/felixfbecker/language-server/bin/php-language-server.php'],
\ }

" UtilSnip
let g:UltiSnipsEditSplit="vertical"

" NCM2
autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect
set shortmess+=c
inoremap <c-c> <ESC>
" let g:UltiSnipsExpandTrigger		= "<Plug>(ultisnips_expand)"
let g:UltiSnipsRemoveSelectModeMappings = 0

"call ncm2#register_source({'name' : 'css',
            "\ 'priority': 9, 
            "\ 'subscope_enable': 1,
            "\ 'scope': ['css', 'scss', 'less'],
            "\ 'mark': 'css',
            "\ 'word_pattern': '[\w\-]+',
            "\ 'complete_pattern': ':\s*',
            "\ 'on_complete': ['ncm2#on_complete#omni',
            "\               'csscomplete#CompleteCSS'],
            "\ })

" Use <TAB> to select the popup menu:
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Syntax checking
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 0
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 1
"let g:syntastic_quiet_messages = { "type": "style" }
