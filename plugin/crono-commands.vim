:command! Vrc :e ~/.config/nvim/init.vim
:command! VrcL :source ~/.config/nvim/init.vim


autocmd FileType nerdtree noremap <buffer> <A-Left> <nop>
autocmd FileType nerdtree noremap <buffer> <A-h> <nop>
autocmd FileType nerdtree noremap <buffer> <A-Right> <nop>
autocmd FileType nerdtree noremap <buffer> <A-l> <nop>

autocmd BufUnload,BufLeave * if empty(&bt) | lclose | endif

"augroup twig_ft
  "au!
  "autocmd BufNewFile,BufRead *.html.twig   set syntax=html
"augroup END

function! CleanClose()
  let todelbufNr = bufnr("%")
  let newbufNr = bufnr("#")
  if ((newbufNr != -1) && (newbufNr != todelbufNr) && buflisted(newbufNr))
      exe "b".newbufNr
  else
      bnext
  endif
  if (bufnr("%") == todelbufNr)
      new
  endif
  exe "bd!".todelbufNr
endfunction


function! SourceAll()
  exe 'source ~/.config/nvim/init.vim'
  for f in split(glob('~/.config/nvim/plugin/*.vim'), '\n')
      exe 'source' f
  endfor
endfunction
