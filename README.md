# CronoVim

## Lightweight and modern Web Development environment.

## Motivation

I ran recently into [SpaceVim](https://spacevim.org), and got really impressed by both the rich development experience they offer, and the overall organization of the project.  
Plus it has great documentation, definitely worth a look if you don't know it yet.  If you're really new to Vim and want a working IDE quickly setup, this is where you should go.

If yo'ure still there, the discovery lead me to rewrite all my dot-files, shamelessly picking into what I saw here and there.

Here is what I got, more lightweight but still IDE-like intended :

* Leader key is `` <Space> ``, shortcuts are organized into namespaces :
  * `` <Space>b `` is for *b*uffers shorcuts
  * `` <Space>bs `` gives you the *s*cratch buffer, `` <Space>bl `` gives you the *l*ocation buffer
  * `` <Space>l `` is for *l*anguage-related shortcuts
  * `` <Space>lu `` adds and "*u*se" or "import" statement, <Space>ld go to *d*efinition
  * You got the idea, see Usage for details
* NERDTree, "airlines", cool visual stuff
* Navigation is a breeze with simple ``<Ctrl>`` / ``<Alt>`` mapping.
* Syntax checking and completion engine for PHP/HTML/CSS/javascript, with LanguageServer


## Requirements

* Linux port only. May accidentaly work on other systems;
* A well capable terminal. I use [Gnome Terminator](https://launchpad.net/terminator);
* A crazy nerd font for your terminal. I use [DejaVu](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/DejaVuSansMono).  
Open the raw file from github, download it, and install it for your OS. Then, makes sure you actually use the font in your terminal preferences.
* neovim, with python3 support  
From inside nvim, try `` :echo has("python3") ``
* PHP > 7.0 for PHP language features;

## Install

* Clone the repository somewhere
* run `` ./install.sh ``
  * It will backup your existing `` ~/.config/nvim `` into  `` ~/.config/nvim_backup `` if it finds one
  * It will symlink itself in place of ~/.config/nvim
* Install Vim-plug (plugin manager)  `` curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim ``
* run `` nvim +PlugInstall ``
* Eventually, [alias](https://doc.ubuntu-fr.org/alias) nvim to vim


## Further customization

Once the initial setup is in place, you want to add your own configuration on top of it, but also
version everything.  
This is an example of what you can do, you should of course adapt it to what you want.

* Create a fresh a repository on your favorite non-M$ git provider.
* From CronoVim directory, add your own configuration files  
  `` touch plugin/custom-keymaps.vim plugin/custom-settings.vim ``
* Switch ownership of CronoVim directory, so it refers to your repository with your custom files.
  `` git remote rename cronovim origin ``  
  `` git remote add origin git@your-reposiory-url.git ``

Rest is up to you. You can sync your dotfiles from time to time, or use git and the "cronovim" remote repo to check if I added anything you like.

## Usage

I use `` <Ctrl> + <Alt> `` modifier for all terminal-related shortcuts, so it leaves me both ``ctrl`` and ``alt`` free inside Vim. If you don't like this setup, you'll want to override navigation with your own settings.

* Navigation between Splits is with `` Alt `` key
  * `` <Alt> + Left (or h) ``
  * `` <Alt> + Right (or l) ``
  * `` <Alt> + Up (or k) ``
  * `` <Alt> + Down (or j) ``

* Navigation between Buffers is with `` Ctrl `` key
  * `` <Ctrl> + Left (or h) ``
  * `` <Ctrl> + Right (or l) ``

* ``<Space>d`` kills current buffer
* ``<Space>sc`` clears the last search
* ``<Space>p`` performs paste from system clipboard
* ``<Space>y`` performs copy to system clipboard

* Completion engine works with 2 forms
  * For simple completion, pressing `` <Enter> `` will insert suggestion
  * For snippet completion (you'll see a `` [+] `` symbol), use ``<Leader>e `` (``e`` stands for *e*xtends)
  * Once you are in expansion mode, use ``<Space>j`` and ``<Space>k`` to navigate between items.

* NERDTree (your favorite file explorer) toggle is F2.  
There are a few important shortcut for NERDTree :  
  * ``m`` prompts for files/directories related commands, I personally use it very often
  * ``B`` toggles the Bookmark view. You can bookmark folders, so you navigate faster between your projects, without leaving Vim. When NERDTree root directory changes, by the way it also changes what is considered the root dir for your vim commands.

* See `` plugin/crono-keymaps.vim `` for full mapping.

For really full documentation, you really should have a look at [NERDCommenter](https://vimawesome.com/plugin/the-nerd-commenter), [Emmet](https://github.com/mattn/emmet-vim) and [Vim-surround](https://vimawesome.com/plugin/surround-vim) for your day-today editing. All 3 of them are present with their default configuration, which is very good.  
Then, have a look at [ncm2](https://github.com/ncm2/ncm2) to go deeper with the completion engine.


Happy coding o/ !
