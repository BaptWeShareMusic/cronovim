#!/bin/bash

if [ -d ~/.config/nvim/ ]; then
   mv ~/.config/nvim ~/.config/nvim_backup
fi

ln -s $PWD ~/.config/nvim
