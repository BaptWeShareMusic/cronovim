# Cheatsheet

## Vim surrounding :

* Use csAB => change surrounding from A to B
* dsA => delete surrounding A
* yssA => add surrounding A. Second s can be a WORD
* Visual + S => add surrounding to visual select
